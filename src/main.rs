use std::io;
use std::io::Read;
use std::io::Write;

fn main() {
	print!("Write 'limit' value *10^5 [10 +- equals 3.5gb]: ");
	io::stdout().flush().unwrap();

	let mut input = String::new();
    io::stdin().read_line(&mut input).unwrap();
    
	let limit: u128 = input.trim().parse().unwrap();
	let limit: u128 = limit * 100_000;
	
	let mut big = vec![];
	for i in 0..1000 {
		big.push(i);
	}
	let big = big;

	println!("Writing...");
	let mut arr = vec![];
	for _ in 1..limit {
		arr.push(big.clone());
	}

	println!("Press enter to exit...");
	let _ = io::stdin().bytes().next();
	println!("Exit...");
}
